// ==UserScript==
// @name         alauda-go-to-localhost
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  link to code without copy id_token!
// @author       gdzang@alauda.io
// @match        *://*/*
// @grant        none
// @require      http://cdn.bootcss.com/jquery/1.12.4/jquery.min.js
// @require      http://code.jquery.com/jquery-latest.js
// ==/UserScript==

(function() {
  "use strict";
  const productMap = {
    "console-acp": "icarus",
    "console-asm": "mephisto",
    "console-devops": "diablo-frontend",
    "console-platform": "underlord"
  };
  if (!usePlugin()) {
    return;
  }
  const triggerButton = $(
    `<div id='trigger-button' style='cursor:pointer;z-index:102;display:inline-block;line-height:30px;opacity:0.5;padding:4px 10px;height:30px;position:fixed;right:0;bottom:0;text-align:center;background-color:red;color:white;'>localhost</div>`
  );

  $("body").append(triggerButton);
  triggerButton.click(trigger);
  $("#trigger-button").animate(
    {
      opacity: "1"
    },
    1000
  );
  function trigger() {
    if (localStorage.getItem("token")) {
      window.location =
        "http://localhost:4200?id_token=" + localStorage.getItem("token");
    } else {
      //刚登录可能不存在token
      $("#trigger-button").text("retry");
    }
  }
  function usePlugin() {
    const { pathname, hostname } = location;
    //already jump
    if (hostname === "localhost") return false;
    const product = productMap[pathname.split("/")[1]];
    if (product) {
      return true;
    }
    return false;
  }
})();
