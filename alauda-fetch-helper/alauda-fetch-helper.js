// ==UserScript==
// @name         alauda-fetch-helper
// @namespace    http://tampermonkey.net/
// @version      0.7
// @description  easily fetch API in alauda product!
// @author       yzli@alauda.io
// @match        *://*/*
// @grant        none
// @require      https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js
// ==/UserScript==

(function() {
    'use strict';
    const uriInput = $(`<div style="padding:2px 10px;"><div>URI:</div><div><textarea id="fetch-uri" rows="3" cols="43" style="max-width: 100%"></textarea></div>`);
    const methodSelector = $(`<div style="padding:2px 10px;"><div>Method:</div><div><select id="method-selector" style="width:100%"><option value ="GET">GET</option><option value ="POST">POST</option><option value ="PUT">PUT</option><option value ="PATCH">PATCH</option><option value ="DELETE">DELETE</option><select/></div>`);
    const fetchBody = $(`<div style="padding:2px 10px;"><div>Body:</div><div><textarea id="fetch-body" rows="3" cols="43" style="max-width: 100%"></textarea></div>`);
    const fetchButton = $(`<button id="fetch-button" style="margin-left: 10px;padding: 5px;">Fetch</button>`);
    const resBody = $(`<div style="padding:2px 10px;"><div>Response Body:</div><textarea id="res-body" rows="5" cols="43" readonly style="background-color:#f9efef;overflow-y: scroll;max-width: 100%"></textarea>`);
    const triggerButton = $(`<div id='fetch-trigger-button' style='cursor:pointer;z-index:102;display:block;width:20px;height:30px;line-height:30px;position:fixed;left:0;top:300px;text-align:center;overflow:visible;background-color:lightgray'>></div>`);
    const fetchBlock = $(`<div id='fetch-block' style='z-index:102;display:none;width:300px;height:520px;line-height:30px;position:fixed;left:25px;top:10px;background-color:lightgray'><h3 style="text-align: center;margin: 0">Fetch Helper</h3><h5 id="image-version" style="text-align: center;margin: 0">Image:</h5></div>`);
	const copyButton = $(`<button id="copy-button" style="margin-left: 10px;padding: 5px;">Copy Response</button>`);
    const clearButton = $(`<button id="clear-button" style="margin-left: 10px;padding: 5px;">Clear</button>`);

    const advancedButton = $(`<button id="advanced-button" style="position:absolute;padding:2px;top:3px;right:3px">高级></button>`);
    const advancedBlock = $(`<div id='advanced-block' style='z-index:102;display:none;width:300px;height:200px;line-height:30px;position:fixed;left:330px;top:10px;background-color:lightgray'><h4 style="text-align: center;margin: 0">高级</h4>`);
    const clusterInput = $(`<div style="padding:2px 10px;"><div>产品部署集群:</div><div><input id="des-cluster" style="width:100%"/></div>`);
    const namespaceInput = $(`<div style="padding:2px 10px;"><div>产品部署namespace:</div><div><input id="des-namespace" style="width:100%"/></div>`);
    const advancedConfirmButton = $(`<button id="advanced-confirm-button" style="margin-left: 10px;padding: 5px;">确定</button>`);

    const productMap = {
        'console-acp': 'icarus',
        'console-asm': 'mephisto',
        'console-devops': 'diablo-frontend',
        'console-platform': 'underlord',
    }
    const defaultConfig = {
        deployCluster: localStorage.getItem('alauda-fetach-helper-cluster') || 'global',
        namespace: localStorage.getItem('alauda-fetach-helper-namespace') || 'cpaas-system',
    }

    $('body').append(triggerButton);
    $('body').append(fetchBlock);
    $('body').append(advancedBlock);
    $('#fetch-block').append(uriInput);
    $('#fetch-block').append(methodSelector);
    $('#fetch-block').append(fetchBody);
    $('#fetch-block').append(fetchButton);
    $('#fetch-block').append(resBody);
    $('#fetch-block').append(copyButton);
    $('#fetch-block').append(clearButton);
    $('#fetch-block').append(advancedButton);
    $('#advanced-block').append(clusterInput);
    $('#advanced-block').append(namespaceInput);
    $('#advanced-block').append(advancedConfirmButton);
    $('#des-cluster').val(defaultConfig.deployCluster);
    $('#des-namespace').val(defaultConfig.namespace);

    triggerButton.click(trigger);
    advancedButton.click(triggerAdvanced);
    fetchButton.click(fetchAction);
    clearButton.click(clearAction);
    copyButton.click(copyAction);
    advancedConfirmButton.click(submitAdvancedSettings);

    function getImageVersion() {
        const product = productMap[location.pathname.split('/')[1]];
        if(!product){ return;}
        const cluster = $('#des-cluster').val();
        const namespace = $('#des-namespace').val();
        const uri = `/kubernetes/${cluster}/apis/extensions/v1beta1/namespaces/${namespace}/deployments/${product}`;
        const method = 'GET';
        const params = {
            method: method,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
        }
        fetch(`${uri}`, params).then(res => res.json()).then(res => {$('#image-version').text(`Image: ${res.spec.template.spec.initContainers[0].image.split('/')[2]}`)});
    }

    function trigger() {
        const isDisplay = fetchBlock.css('display') !== 'none';
        fetchBlock.css('display', isDisplay ? 'none' : 'block');
        if(isDisplay) {
            $('#fetch-trigger-button').text('>');
            $('#advanced-button').text('高级>');
            advancedBlock.css('display', 'none');
        } else {
            getImageVersion();
            $('#fetch-trigger-button').text('<');
        }
    }

    function triggerAdvanced() {
        const isDisplay = advancedBlock.css('display') !== 'none';
        advancedBlock.css('display', isDisplay ? 'none' : 'block');
        if(isDisplay) {
            getImageVersion();
            $('#advanced-button').text('高级>');
        } else {
            $('#advanced-button').text('高级<');
        }
    }

    function submitAdvancedSettings() {
        localStorage.setItem('alauda-fetach-helper-cluster', $('#des-cluster').val());
        localStorage.setItem('alauda-fetach-helper-namespace', $('#des-namespace').val());
        getImageVersion();
    }

    function fetchAction() {
        const uri = $('#fetch-uri').val();
        const method = $('#method-selector').val();
        const body = $('#fetch-body').val();
        if(method === 'GET' || method === 'DELETE') {
            const params = {
                method: method,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            }
            fetch(`${uri}`, params).then(res => res.json()).then(res => {$('#res-body').val(JSON.stringify(res)); console.log(res);});
        } else {
            const params = {
                method: method,
                headers: {
                    'Content-Type': method === 'PATCH' ? 'application/merge-patch+json' : 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: body
            }
            console.log(JSON.parse(body));
            fetch(`${uri}`, params).then(res => res.json()).then(res => {$('#res-body').val(JSON.stringify(res))});
        }
    }

    function copyAction() {
        if($('#res-body').val()){
            $('#res-body').select();
            document.execCommand('copy');
            alert("复制成功!");
        }
    }

    function clearAction() {
        $('#res-body').val('');
    }
})();